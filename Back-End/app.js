const express = require('express');
const bodyParser= require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const bcrypt = require('bcryptjs');
const app = express();
let db = {};

const PARAMS = {
  "db_host": 'mongodb://localhost:2709',
  "db_name": 'schibsted'
}

const authUser = (userEmail, userPass, responseObject, callback) => {
  db.collection('tasks').findOne({email: {$eq: userEmail}}).then((savedUser) => {
    if(savedUser != null && !savedUser.isBlocked){
      bcrypt.compare(userPass, savedUser.pass, (err, isAuthenticated) => callback(err, isAuthenticated, savedUser));
    }else{
      responseObject.status(403).send({"message":"Your account is blocked or does not exist. Contact with our customer support to unlock."})
    }
  })
}

const logWrongPassword = (savedUser, res) => {
  if(savedUser.falsyTries < 5){
    db.collection('tasks').updateOne({"email": savedUser.email}, {$set:{
      falsyTries: (savedUser.falsyTries + 1)
    }}).then((updatedUser) => {
      res.status(404).send({"message": "Wrong password. Please, try again."});
    }).catch((e) => {
      console.error(`Something went wrong while increasing falsy tries: ${e}`);
      res.status(503).send({"message": "Right now all coffee machines stoped. All developers went back to their desks to solve the problem."});
    });
  }else{
    db.collection('tasks').updateOne({"email": savedUser.email}, {$set:{
      isBlocked: true
    }}).then((res) => {
      res.status(403).send({"message": "We have blocked your account. Contact with our customer support to unlock."});
    }).catch((e) => {
      console.error(`Something went wrong while blocking user: ${e}`);
      res.status(503).send({"message": "Right now all coffee machines stoped. All developers went back to their desks to solve the problem."});
    });
  }
}


app.use(bodyParser.urlencoded({extended: true}))

MongoClient.connect(PARAMS["db_host"], {useNewUrlParser: true, useUnifiedTopology: true}, (err, client) => {
  if (err) return console.error(err)
  db = client.db(PARAMS["db_name"])
  app.listen(3000, () => {
    console.log('Listening on port 3000')
  })
})

app.get('/tasks', (req, res) =>{
  const base64Credentials =  req.headers.authorization.split(' ')[1];
  const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
  const [authEmail, authPass] = credentials.split(':');
 
  authUser(authEmail, authPass, res, (err, isAuthenticated, savedUser) => {
    if(isAuthenticated){
      db.collection('tasks').updateOne({"email": authEmail}, {$set:{
        falsyTries: 0
      }}).then((result) => {
        res.status(200).send({
          email: savedUser.email,
          tasks: savedUser.tasks
        })
      }).catch((err) => {
        res.status(503).send({"message": `Right now all coffee machines stoped. All developers went back to their desks to solve the problem. ${err}`});
      });
    }else{
      logWrongPassword(savedUser, res);
    }
  });
});

app.put('/tasks', (req, res) => {
  const incomingBody = req.body;

  authUser(incomingBody.email, incomingBody.pass, res, (err, isAuthenticated, savedUser) => {
    if(isAuthenticated){
      db.collection('tasks').updateOne({"email": incomingBody.email}, {$set:{
        falsyTries: 0,
        tasks: incomingBody.tasks
      }}).then((result) => {
        res.status(200).send({
          email: savedUser.email,
          tasks: incomingBody.tasks
        })
      }).catch((e) => {
        console.error(`Something went wrong while retrieving user: ${e}`);
        res.status(503).send({"message": `Right now all coffee machines stoped. All developers went back to their desks to solve the problem. ${err}`});
      });
    }else{
      logWrongPassword(savedUser, res);
    }
  })
})


app.post('/user', (req, res) => {
  const incomingObject = req.body;
  const hash = bcrypt.hashSync(incomingObject.pass, 10);

  const pagesSimpleObject = {
    email: incomingObject.email,
    pass: hash,
    tasks: incomingObject.tasks,
    falsyTries: 0,
    isBlocked: false
  };

  db.collection('tasks').insertOne(pagesSimpleObject, (err, result) => {
      if (err) return res.status(500).send({"message": err.errmsg});

      res.status(200).send({"message": "Your tasks are safe!"});
  })
})

app.delete('/user', (req, res) => {
  const incomingBody = req.body;

  authUser(incomingBody.email, incomingBody.pass, res, (err, isAuthenticated, savedUser) => {
    if(isAuthenticated){
      db.collection('tasks').deleteOne({"email": incomingBody.email}).then((result) => {
        res.status(200).send({"message": `Account ${incomingBody.email} was deleted`})
      }).catch((e) => {
        console.error(`Something went wrong while deleting user: ${e}`);
        res.status(503).send({"message": `Right now all coffee machines stoped. All developers went back to their desks to solve the problem.`});
      });
    }else{
      logWrongPassword(savedUser, res);
    }
  })
})