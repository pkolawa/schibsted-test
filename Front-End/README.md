# TrackJU
Dashboard made for tasks tracking for Schibsted

To install all dependencies, just run:

### `npm install`

To start the dashboard, you can run (while beeing in the project directory):

### `npm start`

Runs the app in the development mode.<br>

### `npm test`

Launches the test runner in the interactive watch mode. 

### `npm run build`

Builds the app for production to the `build` folder.<br>

*This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).