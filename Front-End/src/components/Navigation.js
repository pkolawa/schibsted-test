import React from 'react';

const Navigation = ({ tasks, deleteTask, updateTask }) => (
    <div className="navigation">
        <h2 className="navigation__title">Tasks list</h2>
        <p className="navigation__subtitle">(click on a task to change its status)</p>
        <ul className="navigation__tasksList">
        {(tasks.length > 0)? tasks.map(task =>
            <li className={"navigation__tasksListItem status--" + task.taskStatus} key={task.taskId}>
                <p className="navigation__taskName">
                    {(task.taskLink === "")? <span>{task.taskName}</span> : <a href={task.taskLink} target="_blank" rel="noopener noreferrer">{task.taskName}</a>}
                </p>
                <div className={"navigation__taskStatus status--" + task.taskStatus} onClick={() => updateTask(task.taskId)}>{task.taskText}</div>
                <p className="navigation__deleteTask" onClick={() => deleteTask(task.taskId)}> DELETE </p>
            </li>
        ): <li className="navigation__subtitle">There is no tasks yet</li>}
        </ul>
    </div>
  )

export default Navigation;