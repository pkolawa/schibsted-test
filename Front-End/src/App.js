import React from 'react';
import './App.scss';
import TasksList from './containers/TasksList';
import AddTask from './containers/AddTask';

function App() {
  return (
    <div className="trackJU">
      <header className="header">
        <h1 className="center-text">Track<span className="redText">JU</span></h1>
        <AddTask />
      </header>
      <main className="main">
        <TasksList />
      </main>
    </div>
  );
}

export default App;
