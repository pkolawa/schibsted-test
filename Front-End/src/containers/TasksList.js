import { connect } from 'react-redux'
import { deleteTask, updateTask } from '../actions'
import Navigation from '../components/Navigation'

const tasksSelector = (state) => {
    if(state.tasks.tasks.length === 0){
        return [];
    }else{
        return state.tasks.tasks
    }
}

const mapStateToProps = state => {
    return {tasks: tasksSelector(state)}
}

const mapDispatchToProps = dispatch => ({
    deleteTask: taskId => dispatch(deleteTask(taskId)),
    updateTask: taskId => dispatch(updateTask(taskId))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation)
