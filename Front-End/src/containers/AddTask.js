import React from 'react'
import { connect } from 'react-redux'
import { addTask } from '../actions'

const AddTask = ({ dispatch }) => {
  let taskNameInput, taskLinkInput

  return (
    <div className="addTask">
        <h2>Add task:</h2>
        <form onSubmit={e => {
            e.preventDefault()
            if (!taskNameInput.value.trim())  {
                return
            }
            dispatch(addTask(taskNameInput.value, taskLinkInput.value))
            taskNameInput.value = ''
            taskLinkInput.value = ''
        }}>
        <div className="addTask__item">
            <label htmlFor="taskNameInput">Task name*:</label>
            <input id="taskNameInput" className="addTask__input" ref={node => taskNameInput = node} />
        </div>
        <div className="addTask__item">
            <label htmlFor="taskLinkInput">Related link:</label>
            <input id="taskLinkInput" className="addTask__input" ref={node => taskLinkInput = node} />
        </div>
        <div className="addTask__inputItem">
            <button className="addTask__submit" type="submit">Add Task</button>
        </div>
        </form>
    </div>
  )
}

export default connect()(AddTask)