let tasks = [];

export const addTask = (taskName, taskLink="#", taskId=Math.floor(Math.random() * 5000), taskStatus=0, taskText="OPEN") => ({
  type: 'ADD_TASK',
  tasks: [...tasks, {taskName,
    taskLink,
    taskId,
    taskStatus, 
    taskText
  }]
})

export const deleteTask = taskId => ({
  type: 'DELETE_TASK',
  taskId: taskId
})

export const updateTask = taskId => ({
  type: 'UPDATE_TASK',
  taskId: taskId
})
