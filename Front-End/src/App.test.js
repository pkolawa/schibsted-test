import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './App';
import rootReducer from './reducers';
import { loadState, saveState } from './localStorage';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const persistedState = loadState();
  const store = createStore(rootReducer, persistedState);
  store.subscribe(() => {
      saveState({
          tasks: store.getState().tasks
      });
  });

  ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
