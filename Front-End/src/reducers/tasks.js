const initialState = {tasks:[]};

const tasks = (state=initialState, action) => {
    switch (action.type) {
      case 'ADD_TASK':
        return {
            ...state,
            tasks: [...state.tasks, action.tasks[0]]
        }
      case 'DELETE_TASK':
        return {
            ...state,
            tasks: state.tasks.filter(task => task.taskId !== action.taskId).map(task => {return task})
        }
      case 'UPDATE_TASK':
        const statusEnum = [
          "OPEN",
          "IN PROGRESS",
          "DONE"
        ];
        
        return {
          ...state,
          tasks: state.tasks.map(task => {
            if(task.taskId === action.taskId){
              const newStatus = task.taskStatus < statusEnum.length-1 ? task.taskStatus+1 : task.taskStatus;
              return {...task, taskStatus: newStatus, taskText: statusEnum[newStatus]}
            } else {
              return task
            } 
          })
        }
      default:
        return state
    }
  }
  
  export default tasks